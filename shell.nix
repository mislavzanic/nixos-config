{ pkgs ? import <nixpkgs> {} }:
with pkgs;
mkShell {
  buildInputs = [
    yaml-language-server
  ];
  shellHook = ''
    export FLAKE="$(pwd)"
  '';
}
