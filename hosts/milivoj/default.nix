{ pkgs, lib, config, ... }:
let configDir = config.dotfiles.configDir;
in {

  imports = [ ./hardware-configuration.nix ];
  networking.hostName = "milivoj";
  time.timeZone = "Europe/Zagreb";

  networking.networkmanager.enable = true;
  networking.useDHCP = false;
  networking.interfaces.wlp2s0.useDHCP = true;

  modules = {
    desktop = {
      xmonad.enable    = true;
      autorandr.enable = true;
      xcompmgr.enable  = true;

      apps = {
        zathura.enable = true;
        spotify.enable = true;
      };

    };

    term = { alacritty.enable = true; };

    shell = {
      zsh.enable      = true;
      direnv.enable   = true;
      starship.enable = true;
      pass.enable     = true;
      gnupg.enable    = true;
    };

    editor = {
      emacs.enable      = true;
    };

    dev = {
      python.enable  = true;
      sh.enable      = true;
      haskell.enable = true;
    };

    services = {
      transmission.enable = true;
    };

    theme.active = "modus-vivendi";
  };

  programs.dconf.enable = true;

  services = {
    blueman.enable = true;
    dbus.packages = with pkgs; [ dconf ];
    fwupd.enable = true;
    xserver = {
      enable = true;
      libinput = {
        enable = true;
        touchpad = {
          naturalScrolling = false;
          middleEmulation = true;
          tapping = true;
        };
      };
    };
  };

  hardware.bluetooth.enable = true;
  sound.enable = true;
  user.packages = with pkgs; [
    openvpn
    slack
    brightnessctl
  ];

  # environment.systemPackages = [
  #   (builtins.getFlake "git+https://codeberg.org/mislavzanic/goaoc")
  # ];

  environment.sessionVariables = rec {
    # alacritty external screen font problem
    WINIT_X11_SCALE_FACTOR = "1.66";
  };

  boot.extraModprobeConfig = ''
    options snd-hda-intel model=dell-headset-multi
    options snd-hda-intel dmic_detect=0
    options usbcore autosuspend=-1
  '';

  virtualisation.docker.enable = true;
  users.users.mzanic.extraGroups = [ "docker" ];
}
