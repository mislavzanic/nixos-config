{ pkgs, lib, config, ... }: {

  imports = [ ./hardware-configuration.nix ];
  networking.hostName = "kreso";
  time.timeZone = "Europe/Zagreb";

  networking.networkmanager.enable = true;
  networking.useDHCP = false;
  networking.interfaces.enp9s0.useDHCP = true;

  modules = {
    desktop = {
      xmonad.enable = true;
      autorandr.enable = true;
      xcompmgr.enable  = true;
      apps = {
        zathura.enable = true;
        dmenu.enable   = true;
        spotify.enable = true;
      };
    };
    term = { alacritty.enable = true; };
    shell = {
      zsh.enable      = true;
      direnv.enable   = true;
      starship.enable = true;
      pass.enable     = true;
      gnupg.enable    = true;
    };
    editor = {
      emacs.enable = true;
    };
    dev = {
      python.enable  = true;
      sh.enable      = true;
      haskell.enable = true;
    };
    services = {
      transmission.enable = true;
      picom.enable        = false;
    };

    theme.active = "modus-vivendi";
  };

  programs.dconf.enable = true;
  services = {
    xserver.enable       = true;
    xserver.videoDrivers = [ "nvidia" ];

    blueman.enable = true;
    dbus.packages = with pkgs; [ dconf ];

    fwupd.enable = true;
  };

  hardware.bluetooth.enable = true;

  environment = {
    systemPackages = with pkgs; [
      transmission
    ];
  };

  nix = {
    settings = {
      trusted-users = [ "mzanic" "root" ];
    };
  };
}
