{-# LANGUAGE PostfixOperators    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Bar where

import Xmobar
    ( defaultConfig,
      Config(font, additionalFonts, bgColor, borderColor, fgColor,
             position, lowerOnStart, hideOnStart, allDesktops, persistent,
             iconRoot, sepChar, alignSep, commands, template),
      XPosition(TopH, BottomH, OnScreen),
      Command(Com),
      Date(Date),
      Monitors(Wireless, Cpu, Memory, CoreTemp, DiskU, Brightness),
      XMonadLog(XPropertyLog),
      Runnable(..) )

import Data.List ( intercalate )
import Config.Helpers ( inColor )
import Config.BarConfig
import Config.Widgets
    ( battery,
      date,
      wireless,
      brightness,
      diskUsage,
      coreTemp,
      memory,
      cpu,
      trayer )

myConfig :: Int -> String -> String -> Config
myConfig n host pos = ( baseConfig n pos )
  { commands        = myCommands n pos
  , template        = myTemplate n pos
  }

myCommands :: Int -> String -> [Runnable]
myCommands n pos = case pos of
  "bottom" -> bottomWidgets n
  _        -> topWidgets n
  where
  topWidgets :: Int -> [Runnable]
  topWidgets n =
    [ Run cpu
    , Run memory
    , Run coreTemp
    , Run diskUsage
    , Run brightness
    , Run $ wireless "wlp2s0"
    , Run date
    , Run $ battery "BAT0"
    , Run $ XPropertyLog $ screenLog n
    ]

  bottomWidgets :: Int -> [Runnable]
  bottomWidgets n =
    [ Run $ XPropertyLog $ screenLog n
    ] <>
    (if n == 0 then
      [Run trayer]
    else
      [Run date]
    )


myTemplate :: Int -> String -> String
myTemplate n pos = case pos of
  "bottom" -> bottomTemplate
  _        -> topTemplate
  where
    bottomTemplate :: String = "%" <> screenLog n <> "% }{" <> (if n == 0 then "%trayerpad%" else "%date%")
    topTemplate :: String = intercalate (inColor myppSepColor "  |  ") leftWidgets <> myScreenLog <> intercalate (inColor myppSepColor "  |  ") rightWidgets
    myScreenLog :: String = "} <icon=haskell.xpm/>" <> inColor myppSepColor "  ::  " <> "%" <> "_XMONAD_LOG_" <> show n <> "% { "
    leftWidgets :: [String] = inColor "#cccccc" <$> [ "%battery%"
                                                    , "%bright%"
                                                    , "%cpu%"
                                                    , "%memory%"
                                                    ]
    rightWidgets :: [String] = inColor "#cccccc" <$> [ "%coretemp%"
                                                     , "%disku%"
                                                     , "%wlp2s0wi%"
                                                     , "%date%"
                                                     ]

    myppSepColor :: String = "#665C54"

getPosition :: Int -> String -> XPosition
getPosition n pos = case pos of
  "bottom" -> OnScreen n (BottomH 24)
  _        -> OnScreen n (TopH 24)

screenLog :: Int -> String
screenLog n = "_XMONAD_LOG_" <> show n
