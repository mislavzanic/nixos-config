{-# LANGUAGE PostfixOperators    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Config.BarConfig where

import Xmobar

baseConfig :: Int -> String -> Config
baseConfig screenId pos = defaultConfig
  { font            = "xft:Cantarell:size=8:antialias=true:autohint=false:style=bold"
  , additionalFonts = [ "xft:FontAwesome:pixelsize=12"
                      , "xft:Font Awesome 5 Free Solid:pixelsize=12"
                      , "xft:Font Awesome 5 Brands:pixelsize=12"
                      ]
  , bgColor         = myppBgColor
  , borderColor     = myppBgColor
  , fgColor         = myppTitle
  , position        = getPosition screenId pos
  , lowerOnStart    = True
  , hideOnStart     = False
  , allDesktops     = True
  , persistent      = True
  , iconRoot        = "/home/mzanic/.config/.dotfiles/config/xmonad/xpm"
  , sepChar         = "%"
  , alignSep        = "}{"
  } where
    myppBgColor :: String = "#000000"
    myppTitle :: String = "#cccccc"

getPosition :: Int -> String -> XPosition
getPosition n pos = case pos of
  "bottom" -> OnScreen n (BottomH 24)
  _        -> OnScreen n (TopH 24)
