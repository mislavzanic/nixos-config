module Config.Helpers where

inIconFont :: String -> String
inIconFont = wrap "<fn=1>" "</fn>"

inColor :: String -> String -> String
inColor color = wrap ("<fc=" <> color <> ">") "</fc>"

inAltIconFont :: String -> String
inAltIconFont = wrap "<fn=2>" "</fn>"

wrap
  :: String  -- ^ left delimiter
  -> String  -- ^ right delimiter
  -> String  -- ^ output string
  -> String
wrap _ _ "" = ""
wrap l r m  = l <> m <> r


seconds, minutes :: Int -> Int
seconds = (* 10)
minutes = (60 *) . seconds
