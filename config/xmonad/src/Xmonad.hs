{-
   __   _____  ___                      _
   \ \ / /|  \/  |                     | |
    \ V / | .  . | ___  _ __   __ _  __| |
    /   \ | |\/| |/ _ \| '_ \ / _` |/ _` |
   / /^\ \| |  | | (_) | | | | (_| | (_| |
   \/   \/\_|  |_/\___/|_| |_|\__,_|\__,_|

-}

module Xmonad ( main ) where


{-# OPTIONS_GHC -Wno-missing-signatures #-}
{-# OPTIONS_GHC -Wno-orphans            #-}

{-# LANGUAGE PostfixOperators    #-}
{-# LANGUAGE StrictData          #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BlockArguments #-}

import App.Alias ( myBorderWidth, myTerminal, myModMask )
import App.Topics ( topics )
import App.Projects

import Bind.Keys ( myKeys )

import Config.Bar ( barSpawner )

import Hooks.ManageHook ( activateHook, myManageHook )
import Hooks.LayoutHook ( myLayout )
import Hooks.HandleEventHook ( myHandleEventHook )
import Hooks.StartupHook ( myStartupHook )
import Hooks.RescreenHook ( myAfterRescreenHook, myRandrChangeHook )
import Theme.Xprop ( base01, baseBorder )

import Prelude

import XMonad ( xmonad
              , (<+>)
              , (-->)
              , XConfig( manageHook
                       , startupHook
                       , layoutHook
                       , handleEventHook
                       , workspaces
                       , borderWidth
                       , terminal
                       , modMask
                       , normalBorderColor
                       , focusedBorderColor
                       , logHook
                       )
              )

-- config
import XMonad.Config.Desktop ( desktopConfig )

-- actions
import XMonad.Actions.SwapPromote (masterHistoryHook)
import XMonad.Actions.TopicSpace (workspaceHistoryHookExclude)
import XMonad.Actions.UpdatePointer (updatePointer)

-- util
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad (scratchpadWorkspaceTag)

-- layout
import XMonad.Layout.NoBorders ( lessBorders, Ambiguity(OnlyScreenFloat) )



-- hooks
import XMonad.Hooks.Rescreen ( addRandrChangeHook, addAfterRescreenHook )
import XMonad.Hooks.ManageDocks ( manageDocks )
import XMonad.Hooks.EwmhDesktops ( setEwmhActivateHook, ewmhFullscreen, ewmh )
import XMonad.Hooks.ManageHelpers ( doFullFloat, isFullscreen )
import XMonad.Hooks.UrgencyHook ( withUrgencyHook, NoUrgencyHook(NoUrgencyHook) )
import XMonad.Hooks.StatusBar ( dynamicEasySBs )
import XMonad.Actions.DynamicProjects (dynamicProjects)
import XMonad.Actions.Prefix (usePrefixArgument)

main :: String -> IO ()
main h = xmonad
       . usePrefixArgument "M-u"
       . setEwmhActivateHook activateHook
       . ewmhFullscreen
       . ewmh
       . addRandrChangeHook myRandrChangeHook
       . addAfterRescreenHook myAfterRescreenHook
       . withUrgencyHook NoUrgencyHook
       . dynamicEasySBs ( pure . ( barSpawner h ) )
       . dynamicProjects projects
       $ myConfig
  where
    myConfig = desktopConfig
      { manageHook         = ( isFullscreen --> doFullFloat ) <+> manageDocks <+> myManageHook <+> manageHook desktopConfig
      , startupHook        = myStartupHook
      , layoutHook         = lessBorders OnlyScreenFloat myLayout
      , handleEventHook    = handleEventHook desktopConfig <+> myHandleEventHook
      , workspaces         = projectNames
      , borderWidth        = myBorderWidth
      , terminal           = myTerminal
      , modMask            = myModMask
      , normalBorderColor  = baseBorder
      , focusedBorderColor = base01
      , logHook            = workspaceHistoryHookExclude [scratchpadWorkspaceTag]
                             <> masterHistoryHook
                             <> updatePointer (0.5, 0.5) (0, 0)
      }
      `additionalKeysP` myKeys
