-- |

module Bind.Keys where

import App.Alias ( myPDF, myBrowser, myFont, myTerminal )
import App.Scratchpad ( myScratchPads )
import App.Projects ( projects, spawnEditorInProject, toggleProject, spawnAction )
import App.Topics ( topicConfig
                  , topics, spawnTermInTopic
                  , spawnEditorInTopic
                  , goto
                  , promptedGoto
                  , promptedShift
                  , toggleTopic
                  )
import Config.Prompts ( promptTheme
                      , torrentPrompt
                      , bookPrompt
                      , searchList
                      )
import Theme.Xprop ( basebg, basefg )

import qualified Data.Map as M
import qualified XMonad.StackSet as W

import XMonad ( X
              , Default(def)
              , button1
              , button2
              , button3
              , noModMask
              , xK_a
              , xK_b
              , xK_d
              , xK_f
              , xK_p
              , xK_r
              , spawn
              , whenJust
              , focus
              , kill
              , mouseMoveWindow
              , mouseResizeWindow
              , sendMessage
              , windows
              , withFocused
              , KeyMask
              , KeySym
              , Rectangle( Rectangle
                         , rect_y
                         , rect_x
                         , rect_width
                         , rect_height
                         )
              , XConfig( XConfig
                       , modMask
                       )
              , ChangeLayout( NextLayout )
              , Resize( Expand
                      , Shrink
                      )
              )


import XMonad.Actions.CycleWS ( moveTo
                              , nextWS
                              , prevWS
                              , shiftNextScreen
                              , shiftPrevScreen
                              , swapNextScreen
                              , swapPrevScreen
                              , WSType(..)
                              , Direction1D( Next
                                           , Prev
                                           )
                              , emptyWS
                              , ignoringWSs, nextScreen, prevScreen
                              )
import XMonad.Actions.EasyMotion ( selectWindow )
import XMonad.Actions.GridSelect ( goToSelected )
import XMonad.Actions.Promote ( promote )
import XMonad.Actions.Search ( promptSearch )
import XMonad.Actions.Submap ( visualSubmap )

import XMonad.Hooks.ManageDocks ( ToggleStruts(..) )

import XMonad.Layout.WorkspaceDir ( changeDir )
import XMonad.Layout.MultiToggle (Toggle (Toggle))
import XMonad.Layout.MultiToggle.Instances ( StdTransformers(NBFULL, NOBORDERS) )
import XMonad.Layout.SubLayouts ( mergeDir, GroupMsg(UnMerge) )

import XMonad.Util.NamedScratchpad ( namedScratchpadAction, scratchpadWorkspaceTag, toggleDynamicNSP, dynamicNSPAction )

import XMonad.Prompt ( XPConfig )

import XMonad.Prompt.Shell ( shellPrompt )
import XMonad.Prompt.Window ( allWindows
                            , windowPrompt
                            , WindowPrompt( Bring
                                          , Goto
                                          ), windowMultiPrompt, wsWindows
                            )

import XMonad.Util.XUtils ( WindowRect (CustomRect)
                          , WindowConfig ( WindowConfig
                                         , winBg
                                         , winFg
                                         , winFont
                                         , winRect
                                         )
                          )
import XMonad.Actions.DynamicProjects (switchProjectPrompt, shiftToProjectPrompt, switchProject, shiftToProject, changeProjectDirPrompt)
import XMonad.Layout.Spacing (toggleWindowSpacingEnabled, toggleScreenSpacingEnabled)
import XMonad.Actions.Prefix (withPrefixArgument, PrefixArgument (Raw))

type Keybind = (String, X ())

windowsKeys :: [Keybind]
windowsKeys =
  [ ("M-j", windows W.focusDown)
  , ("M-k", windows W.focusUp)
  , ("M-m", windows W.focusMaster)

  , ("M-w", nextScreen)
  , ("M-e", prevScreen)
  , ("M-C-w", swapNextScreen)
  , ("M-C-e", swapPrevScreen)

  , ("M-S-w", shiftNextScreen)
  , ("M-S-e", shiftPrevScreen)

  , ("M-b", goToSelected def)
  , ("M-f", selectWindow def >>= (`whenJust` windows . W.focusWindow))
  , ("M-<Backspace>", promote)

  , ("M-S-j", windows W.swapDown)
  , ("M-S-k", windows W.swapUp)


  , ("M-h", sendMessage Shrink)
  , ("M-l", sendMessage Expand)

  , ("M-;"  , withFocused $ sendMessage . mergeDir id)
  , ("M-C-;"  , withFocused (sendMessage . UnMerge) *> windows W.focusUp)
  ]

scratchpadKeys :: [(String, X ())]
scratchpadKeys =
  [ ("M-C-<Return>", namedScratchpadAction myScratchPads "terminal")
  , ("M-C-s", namedScratchpadAction myScratchPads "spotify")
  , ("M-C-t", namedScratchpadAction myScratchPads "org-todo")
  , ("M-C-u", namedScratchpadAction myScratchPads "emacs-scratch")
  ] ++
  [ ("M1-C-" ++ num, withFocused . toggleDynamicNSP $ "dyn" ++ num)
  | num <- map show [1 .. 9 :: Int]
  ] ++
  [ ("M-C-" ++ num, dynamicNSPAction $ "dyn" ++ num)
  | num <- map show [1 .. 9 :: Int]
  ]

projectKeys :: [(String, X ())]
projectKeys =
  [ ("M-a", withPrefixArgument $
              \case Raw _ -> spawn myTerminal >> spawnEditorInProject
                    _     -> spawnAction
    )
  , ("M-g", switchProjectPrompt promptTheme)
  , ("M-S-g", shiftToProjectPrompt promptTheme)
  , ("M1-<Tab>", toggleProject)
  , ("M1-u", spawnEditorInProject)
  , ("M-<Return>", spawn myTerminal)

  , ("M1-j", moveTo Next ( Not emptyWS :&: ignoringWSs [scratchpadWorkspaceTag] ))
  , ("M1-k", moveTo Prev ( Not emptyWS :&: ignoringWSs [scratchpadWorkspaceTag] ))
  , ("M1-C-j", nextWS)
  , ("M1-C-k", prevWS)
  ] ++
  [ ("M-" ++ m ++ k, f i)
  | (i, k) <- zip projects (map show [1 .. 9 :: Int])
  , (f, m) <- [(switchProject, ""), (shiftToProject, "S-")]
  ]


promptKeys :: [(String, X ())]
promptKeys =
  [ ("M-p", shellPrompt promptTheme)
  , ("M-o", visualSubmap winConfig . promptList $ promptTheme)
  , ("M-[", windowMultiPrompt promptTheme [(Goto, allWindows), (Goto, wsWindows)])
  , ("M-]", windowPrompt promptTheme Bring allWindows)
  , ("M1-s", visualSubmap winConfig $ searchList $ promptSearch promptTheme)
  , ("M-d",  changeProjectDirPrompt promptTheme)

  , ("M-s", visualSubmap winConfig . M.fromList $ [ ((noModMask, xK_f), ("Fullscreen screenshot", spawn "takeScreenshot.sh Fullscreen"))
                                                  , ((noModMask, xK_r), ("Region screenshot", spawn "takeScreenshot.sh Region"))
                                                  , ((noModMask, xK_a), ("Active window screenshot", spawn "takeScreenshot.sh Active Window"))
                                                  ])
  ]

layoutKeys :: [Keybind]
layoutKeys =
  [ ("M-<Tab>", sendMessage NextLayout)
  , ("M-<Space>", sendMessage (Toggle NBFULL) >> sendMessage ToggleStruts)
  , ("M-C-b"  , sendMessage $ Toggle NOBORDERS)
  , ("M-C-g"  , toggleWindowSpacingEnabled >> toggleScreenSpacingEnabled)
  ]

appKeys :: [(String, X ())]
appKeys =
  [ ("M-q", kill)
  , ("M-S-r", spawn "xmonad --restart")


  , ("<XF86AudioRaiseVolume>", spawn "pamixer -i 5 && notify-send -u low -t 1500 $(pamixer --get-volume)")
  , ("<XF86AudioLowerVolume>", spawn "pamixer -d 5 && notify-send -u low -t 1500 $(pamixer --get-volume)")
  , ("<XF86AudioMute>", spawn "pamixer -t")

  , ("<XF86MonBrightnessDown>", spawn "brightnessctl s $(($(brightnessctl g) - 50))")
  , ("<XF86MonBrightnessUp>",   spawn "brightnessctl s $(($(brightnessctl g) + 50))")

  , ("M1-w", spawn myBrowser)
  , ("M1-z", spawn myPDF)
  , ("M1-l", spawn "dm-tool lock")

  , ("M-S-w", spawn "sxiv -r -q -t -o ~/.local/share/wall/*")
  ]

myKeys :: [(String, X ())]
myKeys = concat
  [ appKeys
  , layoutKeys
  , promptKeys
  , windowsKeys
  , projectKeys
  , scratchpadKeys
  ]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
------------------------------------------------------------------------
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), \w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster)

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), \w -> focus w >> windows W.shiftMaster)

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), \w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster)

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

promptList :: XPConfig -> M.Map (KeyMask, KeySym) (String, X ())
promptList theme = M.fromList [ ((noModMask, xK_p), ("MagnetLink prompt", torrentPrompt theme))
                              , ((noModMask, xK_b), ("PDF reader prompt", bookPrompt theme))
                              , ((noModMask, xK_d), ("Change working dir", changeDir theme))
                              ]
winConfig :: WindowConfig
winConfig = WindowConfig { winBg = basebg
                         , winFg = basefg
                         , winFont = myFont
                         , winRect = CustomRect Rectangle { rect_y = -40
                                                          , rect_x = 1600
                                                          , rect_width = 350
                                                          , rect_height = 430
                                                          }
                         }
