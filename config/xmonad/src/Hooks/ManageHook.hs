module Hooks.ManageHook where

import App.Scratchpad ( myScratchPads )
import App.Topics ( topics )

import XMonad
    ( (<+>),
      doShift,
      doFloat,
      appName,
      doIgnore,
      resource,
      composeAll,
      (-->),
      className,
      (=?),
      ManageHook )

import XMonad.Actions.TopicSpace ( topicNames )

import XMonad.Hooks.ManageHelpers ( isFullscreen
                                  , doFullFloat
                                  , doFocus
                                  , isDialog
                                  , doCenterFloat
                                  )
import XMonad.Hooks.UrgencyHook ( doAskUrgent )

import XMonad.Util.NamedScratchpad ( namedScratchpadManageHook )

activateHook :: ManageHook
activateHook = mconcat
    [ isDialog --> doAskUrgent
    , className =? "mpv" --> doAskUrgent
    , className =? "help" --> doAskUrgent
    , className =? "Sxiv" --> doAskUrgent
    , className =? "discord" --> doAskUrgent
    , className =? "Zathura" --> doAskUrgent
    , className =? "Emacs" --> doAskUrgent
    , pure True --> doFocus
    ]

myManageHook :: ManageHook
myManageHook = composeAll
    [ resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore

    , appName   =? "pavucontrol"      --> doCenterFloat
    , className =? "Virt-manager"     --> doFloat
    , className =? "Thunderbird"      --> doFloat
    , className =? "Nvidia-settings"  --> doFloat
    , className =? "Thunar"           --> doFloat

    , className =? "Brave-browser" --> doShift (myWorkspaces !! 1)
    , className =? "Slack"         --> doShift (myWorkspaces !! 8)

    , isFullscreen --> doFullFloat
    ] <+> namedScratchpadManageHook myScratchPads
  where
    myWorkspaces :: [String]
    myWorkspaces = topicNames topics
