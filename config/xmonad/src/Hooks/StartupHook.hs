-- |

module Hooks.StartupHook where

import Theme.Xprop ( basebg )

import XMonad ( spawn, X, xC_left_ptr )

import XMonad.Hooks.SetWMName (setWMName)

import XMonad.Util.Cursor (setDefaultCursor)
import XMonad.Util.SpawnOnce (spawnOnce)


trayerColor :: String -> String
trayerColor c = "--tint 0x" ++ tail c

myStartupHook :: X()
myStartupHook = do
  spawn     "killall trayer"
  spawn     ("sleep 2 && trayer --edge bottom --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor primary --transparent true --alpha 0 " ++ trayerColor basebg ++ " --height 22 &")

  spawnOnce "[ -f ~/.local/share/bg ] && feh --bg-fill ~/.local/share/bg"
  spawnOnce "emacs-28.1 --daemon &"
  spawnOnce "dunst &"
  spawnOnce "nm-applet &"
  spawnOnce "pasystray &"
  spawnOnce "xsetroot -cursor_name left_ptr &"
  spawnOnce "xmodmap ~/.config/.dotfiles/config/x11/Xmodmap"
  spawnOnce "~/.config/xmonad/xinit"

  setDefaultCursor xC_left_ptr
  setWMName "LG3D"


-- trayerStartup :: [String]
-- trayerStartup = [ "killall trayer"
--                 , "sleep 2 && trayer --edge bottom --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor primary --transparent true --alpha 0 "
--                   ++ trayerColor basebg
--                   ++ " --height 22 &"
--                 ]

-- spawnThisOnce :: [String]
-- spawnThisOnce = [ "[ -f ~/.local/share/bg ] && feh --bg-fill ~/.local/share/bg"
--                 , "emacs-28.1 --daemon &"
--                 ]

-- startup :: X()
-- startup = mconcat [ mconcat (spawn <$> trayerStartup)
--                   , mconcat (spawnOnce <$> spawnThisOnce)
--                   , setWMName "LG3D"
--                   ]
