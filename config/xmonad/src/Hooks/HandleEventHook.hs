-- |

module Hooks.HandleEventHook where

import XMonad ( (=?), className )

import XMonad.Hooks.WindowSwallowing ( swallowEventHook )

myHandleEventHook = swallowEventHook (className =? "Alacritty") (return True)
