{-# LANGUAGE ScopedTypeVariables #-}

module Hooks.LayoutHook where

import Data.Ratio ( (%) )

import Theme.Xprop ( base08, base01, baseBorder, base00 )

import XMonad.Hooks.ManageDocks ( avoidStruts )

import XMonad ( Full(Full), Default(def) )

import XMonad.Layout.BoringWindows ( boringAuto )
import XMonad.Layout.LayoutCombinators ( (|||) ) -- jump to layout
import XMonad.Layout.LayoutModifier ( ModifiedLayout )
import XMonad.Layout.LimitWindows (limitWindows)
import XMonad.Layout.Magnifier ( magnify, MagnifyThis(NoMaster) )
import XMonad.Layout.MultiToggle ( mkToggle, (??), EOT(EOT) )
import XMonad.Layout.MultiToggle.Instances (StdTransformers (NBFULL, NOBORDERS))
import XMonad.Layout.NoBorders ( noBorders )
import XMonad.Layout.PerWorkspace ( onWorkspace, onWorkspaces )
import XMonad.Layout.Renamed ( renamed, Rename(CutWordsLeft, Replace) )
import XMonad.Layout.ResizableTile ( ResizableTall(ResizableTall) )
import XMonad.Layout.Simplest (Simplest (Simplest))
import XMonad.Layout.Spacing ( spacingRaw, Border(Border), Spacing )
import XMonad.Layout.SubLayouts ( subLayout )
import XMonad.Layout.Tabbed ( addTabs
                            , Theme(..)
                            , Shrinker(..), shrinkText
                            )
import XMonad.Layout.ThreeColumns ( ThreeCol(ThreeColMid) )
import App.Alias (myFont)
import App.Projects (workRepos, wmHacking)
import XMonad.Actions.DynamicProjects (Project(projectName))
import XMonad.Layout.PerScreen (ifWider)

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

setName :: String -> l a -> ModifiedLayout Rename l a
setName n = renamed [Replace n]

rTall :: Int -> Rational -> Rational -> ResizableTall l
rTall m r c = ResizableTall m r c []

myLayout = layoutOpts
  $ onWorkspaces hackingWorkspaces (ifWider 1920 (tcm ||| full) $ hack ||| full)
  $ onWorkspaces tiledWorkspaces (ifWider 1920 (tcm ||| full) $ tiled ||| full)
  $ onWorkspace "5:vid" full
  $ ifWider 1920 (tcm ||| full)
  $ tiled ||| hack ||| full where
     layoutOpts = cutWords 1 . addTabbed . toggleBorders . avoidStruts

     tcm = rn "ThreeColMid"
         $ mySpacing gapSize
         $ ThreeColMid 1 (3/100) (3/7)

     hack = rn "Hack"
          $ mySpacing gapSize
          $ limitWindows 3
          $ magnify 1.3 (NoMaster 3) True
          $ rTall 1 (3 % 100) (13 % 25)

     full = rn "Full" -- "Full"
          $ noBorders Full

     -- tiled
     tiled = rn "Tiled"--"Tall"
           $ mySpacing gapSize
           $ rTall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

     toggleBorders = mkToggle $ NBFULL ?? NOBORDERS ?? EOT
     addTabbed = boringAuto . addTabs shrinkText tabTheme . subLayout [] Simplest

     rn :: String -> l a -> ModifiedLayout Rename l a
     rn s = renamed [Replace s]

     gapSize :: Integer = 4

     hackingWorkspaces :: [String]
     hackingWorkspaces = map projectName (workRepos ++ wmHacking) <> ["6:dots" , "8:work"]

     tiledWorkspaces :: [String]
     tiledWorkspaces =
       [ "1:web"
       , "2:brave"
       , "3"
       , "4"
       , "7:rss"
       , "9:slack"
       , "notes"
       , "k9s"
       ]


data EmptyShrinker = EmptyShrinker deriving (Read, Show)
instance Shrinker EmptyShrinker where
    shrinkIt _ _ = []

cutWords :: Int -> l a -> ModifiedLayout Rename l a
cutWords i = renamed [CutWordsLeft i]

tabTheme :: Theme
tabTheme = def
  { activeColor = base01
  , urgentColor = base01
  , inactiveColor = baseBorder
  , activeBorderColor = base01
  , inactiveBorderColor = baseBorder
  , activeTextColor     = base00
  , inactiveTextColor   = base01
  , fontName = myFont
  }
