-- |

module App.Topics where

import App.Alias ( myEditor, myTerminal )
import Config.Prompts ( promptTheme )

import XMonad ( X
              , Default(def)
              , spawn
              , windows
              )

import XMonad.Actions.TopicSpace ( tiActions
                                 , tiDirs
                                 , Topic
                                 , TopicItem (TI)
                                 , TopicConfig ( topicDirs
                                               , topicActions
                                               , defaultTopicAction
                                               , defaultTopic
                                               )
                                 , currentTopicDir
                                 , inHome
                                 , noAction
                                 , shiftNthLastFocused
                                 , switchNthLastFocusedByScreen
                                 , switchTopic
                                 )

import XMonad.Prompt.Workspace (workspacePrompt)

import XMonad.Util.Run ( (>-$)
                       , inEditor
                       , proc
                       , termInDir
                       )

import qualified XMonad.StackSet as W

topicConfig :: TopicConfig
topicConfig = def
  { topicDirs          = tiDirs    topics
  , topicActions       = tiActions topics
  , defaultTopicAction = const (pure ())
  , defaultTopic       = "1:web"
  }

topics :: [TopicItem]
topics =
  [ inHome   "1:web"                                              (spawn "firefox")
  , inHome   "2:brave"                                            (spawn "brave")
  , only     "3"
  , only     "4"
  , noAction "5:vid"    "~/.local/torrents"
  , TI       "6:dots"   ".config/.dotfiles"                       spawnEditorInTopic
  , TI       "7:rss"    "~"                                       (spawn $ myEditor <> " --eval '(elfeed)'")
  , TI       "8:work"   "~/.local/work"                           spawnProgs
  , TI       "9:slack"  "~"                                       (spawn "slack")
  ] <>
  map ($ spawnEditorInTopic)
  [ TI       "xmn"      "~/.config/.dotfiles/config/xmonad"
  , TI       "xmb"      "~/.config/.dotfiles/config/xmobar"
  , TI       "notes"    "~/.local/org_roam"
  ] <>
  map ($ spawnProgs)
  [ TI       "dev"      ".local/dev"
  , TI       "gotsm"    ".local/dev/gotsm"
  , TI       "tfm"      ".local/work/repos/tf"
  , TI       "helm"     ".local/work/repos/argoCD"
  ] <>
  [ TI       "k9s"      "~/.local/work/repos/argoCD"              (spawn $ myTerminal <> " --command nix-shell -p k9s -p google-cloud-sdk -p kubectl --command k9s")
  , TI       "aoc"      "~/.local/dev/aoc"                        spawnAOC
  , TI       "pdf"      "~/.local/books"                          (spawn "zathura")
  ]
  where
    only :: Topic -> TopicItem
    only n = noAction n "."

    spawnProgs :: X ()
    spawnProgs = spawnTermInTopic *> spawnEditorInTopic

    spawnAOC :: X ()
    spawnAOC = spawnEditorInTopic *> spawn "firefox adventofcode.com"

spawnTermInTopic :: X ()
spawnTermInTopic = proc $ termInDir >-$ currentTopicDir topicConfig

spawnEditorInTopic :: X ()
spawnEditorInTopic = proc $ inEditor >-$ currentTopicDir topicConfig

goto :: Topic -> X ()
goto = switchTopic topicConfig

promptedGoto :: X ()
promptedGoto = workspacePrompt promptTheme goto

promptedShift :: X ()
promptedShift = workspacePrompt promptTheme $ windows . W.shift

toggleTopic :: X ()
toggleTopic = switchNthLastFocusedByScreen topicConfig 1

shiftToLastTopic :: X ()
shiftToLastTopic = shiftNthLastFocused 1
