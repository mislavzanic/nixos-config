module App.Scratchpad ( myScratchPads ) where

import App.Alias ( myTerminal
                 , myEditor
                 )

import XMonad ( className
              , (<||>)
              , (=?)
              , title
              )

import XMonad.Util.NamedScratchpad ( customFloating
                                   , NamedScratchpad(NS)
                                   )

import qualified XMonad.StackSet as W

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "tremc" spawnTremc findTremc manageTremc
                , NS "spotify" spawnSpotify findSpotify manageSpotify
                , NS "emacs-scratch" spawnEmacs findEmacs manageEmacs
                , NS "org-todo" spawnTodos findTodos manageEmacs
                ]
  where
    spawnTerm  = myTerminal ++ " -t scratchpad"
    findTerm   = title =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
                 where
                   h = 0.9
                   w = 0.9
                   t = 0.95 - h
                   l = 0.95 - w
    spawnTremc  = myTerminal ++ " -t tremc -e nix-shell -p tremc --command tremc"
    findTremc   = title =? "tremc"
    manageTremc = customFloating $ W.RationalRect l t w h
                 where
                   h = 0.9
                   w = 0.9
                   t = 0.95 - h
                   l = 0.95 - w
    spawnSpotify  = "spotify"
    findSpotify   = title =? "Spotify" <||> title =? "spotify" <||> className =? "Spotify" <||> title =? ""
    manageSpotify = customFloating $ W.RationalRect l t w h
                    where
                      h = 0.9
                      w = 0.9
                      t = 0.95 - h
                      l = 0.95 - w
    spawnEmacs  = myEditor ++ " --eval '(dashboard-refresh-buffer)' -F '(quote (name . \"emacs-scratch\"))'"
    findEmacs   = title =? "emacs-scratch"
    manageEmacs = customFloating $ W.RationalRect l t w h
                 where
                   h = 0.9
                   w = 0.9
                   t = 0.95 - h
                   l = 0.95 - w
    spawnTodos  = myEditor ++ " --eval '(org-todo-list)' -F '(quote (name . \"org-todo\"))'"
    findTodos   = title =? "org-todo"
