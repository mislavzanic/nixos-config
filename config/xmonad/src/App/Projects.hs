-- |

module App.Projects where

import App.Alias ( myEditor, myTerminal )

import XMonad

import qualified XMonad.StackSet           as W

import XMonad.Actions.DynamicProjects
    ( currentProject,
      lookupProject,
      switchProject,
      Project(..),
      ProjectName )
import XMonad.Hooks.WorkspaceHistory ( workspaceHistoryByScreen )


import XMonad.Util.Run ( (>-$)
                       , (>->)
                       , inEditor
                       , proc, setFrameName
                       )

import Data.Maybe (fromMaybe, listToMaybe)

defaultProject :: Project
defaultProject = inHome "1:web" ( Just $ spawn "firefox" )

projectNames :: [ProjectName]
projectNames = map projectName projects

inHome :: ProjectName -> Maybe (X ()) -> Project
inHome pn = Project pn "~"

mainProjects :: [Project]
mainProjects =
  [ inHome "1:web"                            ( Just $ spawn "firefox" )
  , inHome "2:brave"                          ( Just $ spawn "brave" )
  , inHome "3"                                Nothing
  , inHome "4"                                Nothing
  , Project "5:vid"    "~/.local/torrents"    ( Just $ spawn myTerminal )
  , Project "6:dots"   "~/.config/.dotfiles"  ( Just spawnEditorInProject )
  , inHome  "7:rss"                           ( Just $ spawn $ myEditor <> " --eval '(elfeed)'" )
  , Project "8:work"   "~/.local/work"        ( Just spawnEditorInProject )
  , inHome  "9:slack"                         ( Just $ spawn "slack" )
  , Project "notes"    "~/.local/org_roam"    ( Just spawnEditorInProject )
  ]

wmHacking :: [Project]
wmHacking =
  map ($ Just spawnEditorInProject)
  [ Project "xmn"      "~/.config/.dotfiles/config/xmonad"
  , Project "xmb"      "~/.config/.dotfiles/config/xmobar"
  ]

workRepos :: [Project]
workRepos =
  map ($ Just spawnProgs)
  [ Project "dev"      "~/.local/dev"
  , Project "tfm"      "~/.local/work/repos/tf"
  , Project "helm"     "~/.local/work/repos/argoCD"
  ]
  where
    spawnProgs :: X ()
    spawnProgs = (spawn myTerminal) *> spawnEditorInProject

k9sWorkspace :: [Project]
k9sWorkspace =
  [ Project "k9s"      "~/.local/work/repos/argoCD" (Just $ spawn $ myTerminal <> " --command nix-shell -p k9s -p google-cloud-sdk -p kubectl --command k9s")]

projects :: [Project]
projects = mainProjects <> wmHacking <> workRepos <> k9sWorkspace

spawnEditorInProject :: X ()
spawnEditorInProject = do
  project <- currentProject
  proc $ inEditor >-$ pure ( projectDirectory project )

spawnNamedEditorInProject :: String -> X()
spawnNamedEditorInProject name = do
  project <- currentProject
  proc $ inEditor >-> setFrameName name >-$ pure ( projectDirectory project )


spawnAction :: X ()
spawnAction = do
  proj <- currentProject
  fromMaybe (spawn myTerminal) (projectStartHook proj)

toggleProject :: X ()
toggleProject = switchToNthLastProjFocusedByScreen defaultProject 1

-- shamelessly stolen from XMonad.Actions.TopicSpace
switchToNthLastProjFocusedByScreen :: Project -> Int -> X ()
switchToNthLastProjFocusedByScreen defProj depth = do
  sid <- gets $ W.screen . W.current . windowset
  sws <- fromMaybe []
       . listToMaybe
       . map snd
       . filter ((== sid) . fst)
     <$> workspaceHistoryByScreen
  project <- lookupProject ((sws ++ repeat (projectName defProj)) !! depth )
  switchProject $ fromMaybe defProj project
