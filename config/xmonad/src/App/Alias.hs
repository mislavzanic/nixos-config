{-# LANGUAGE ScopedTypeVariables #-}

module App.Alias ( myModMask
                 , myTerminal
                 , myEditor
                 , myPDF
                 , myBrowser
                 , myFont
                 , myBorderWidth
                 ) where

import XMonad ( mod4Mask
              , Dimension
              , KeyMask
              )

import Prelude
import XMonad.Actions.Search ( Browser )

myModMask     :: KeyMask   = mod4Mask
myTerminal    :: String    = "alacritty"
myEditor      :: String    = "emacsclient -c -a'emacs'"
myPDF         :: String    = "zathura"
myBrowser     :: Browser   = "firefox"
myFont        :: String    = "xft:Cantarell:size=8:antialias=true:autohint=false:style=bold"
myBorderWidth :: Dimension = 2
