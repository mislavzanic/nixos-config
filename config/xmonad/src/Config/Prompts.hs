-- |

module Config.Prompts where

import App.Alias ( myFont )

import Theme.Xprop ( basebg, basefg, base01 )

import GHC.Exts (fromList)
import Data.Map.Strict (Map)

import XMonad ( X
              , Default(def)
              , noModMask
              , xK_a
              , xK_d
              , xK_g
              , xK_h
              , xK_i
              , xK_k
              , xK_r
              , xK_s
              , xK_w
              , xK_x
              , xK_y
              , spawn
              , ButtonMask
              , KeySym
              )

import XMonad.Util.Run ( runProcessWithInput )

import XMonad.Actions.Search ( duckduckgo
                             , github
                             , hoogle
                             , imdb
                             , promptSearch
                             , promptSearchBrowser'
                             , searchEngine
                             , searchEngineF
                             , selectSearchBrowser
                             , stackage
                             , wikipedia
                             , youtube
                             , Browser
                             , SearchEngine (..)
                             )

import XMonad.Prompt ( Direction1D ( Next
                                   , Prev
                                   )
                     , XPConfig (font
                                , bgColor
                                , fgColor
                                , bgHLight
                                , fgHLight
                                , borderColor
                                , promptBorderWidth
                                , position
                                , height
                                , historyFilter
                                , defaultText
                                , showCompletionOnTab
                                , alwaysHighlight
                                , maxComplRows
                                , searchPredicate
                                , sorter
                                , historySize
                                , autoComplete
                                , promptKeymap
                                )
                     , deleteString
                     , endOfLine
                     , killAfter
                     , killBefore
                     , killWord
                     , mkComplFunFromList
                     , moveCursor
                     , moveHistory
                     , moveWord
                     , pasteString
                     , quit
                     , setDone
                     , setSuccess
                     , startOfLine
                     , XP
                     , XPPosition(Top)
                     , emacsLikeXPKeymap
                     )

import XMonad.Prompt.FuzzyMatch ( fuzzyMatch, fuzzySort )
import XMonad.Prompt.Input ( (?+)
                           , inputPrompt
                           , inputPromptWithCompl
                           )

import XMonad.Prompt.Window ( WindowPrompt ( Goto
                                           , Bring
                                           )
                            , allWindows
                            , windowPrompt
                            )

promptTheme :: XPConfig
promptTheme = def
      { font                = myFont
      , bgColor             = basebg
      , fgColor             = basefg
      , bgHLight            = base01
      , fgHLight            = basebg
      , borderColor         = basebg
      , promptBorderWidth   = 0
      , position            = Top
      , height              = 20
      , historySize         = 256
      , promptKeymap        = emacsLikeXPKeymap
      , historyFilter       = id
      , defaultText         = []
      , showCompletionOnTab = False
      , searchPredicate     = fuzzyMatch
      , alwaysHighlight     = True
      , maxComplRows        = Nothing      -- set to Just 5 for 5 rows
      }

torrentPrompt :: XPConfig -> X ()
torrentPrompt c = inputPrompt c "Torrent" ?+ \magnet -> spawn $ "transmission-remote -a \"" ++ magnet ++ "\""

bookPrompt :: XPConfig -> X ()
bookPrompt c = do
    exes <- runProcessWithInput "get_books.sh" [] ""
    inputPromptWithCompl c "Books" (mkComplFunFromList
                                             def { searchPredicate = fuzzyMatch
                                                 , sorter = fuzzySort
                                                 }
                                             $ lines exes) ?+ \book -> spawn $ "zathura " ++ book

kubernetes, reddit, archWiki, xmonadDocs :: SearchEngine
reddit     = searchEngine "reddit" "https://www.reddit.com/search/?q="
archWiki   = searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="
kubernetes = searchEngine "kubernetes" "https://kubernetes.io/search/?q="
xmonadDocs = searchEngine "xmonad" "https://xmonad.github.io/xmonad-docs/"

searchList :: (SearchEngine -> a) -> Map (ButtonMask, KeySym) (String, a)
searchList m = fromList [ ((noModMask, xK_a), ("archwiki", m archWiki))
                        , ((noModMask, xK_d), ("duckduckgo", m duckduckgo))
                        , ((noModMask, xK_g), ("github", m github))
                        , ((noModMask, xK_h), ("hoogle", m hoogle))
                        , ((noModMask, xK_i), ("imdb", m imdb))
                        , ((noModMask, xK_k), ("kubernetes", m kubernetes))
                        , ((noModMask, xK_r), ("reddit", m reddit))
                        , ((noModMask, xK_s), ("stackage", m stackage))
                        , ((noModMask, xK_w), ("wikipedia", m wikipedia))
                        , ((noModMask, xK_x), ("xmonad", m xmonadDocs))
                        , ((noModMask, xK_y), ("youtube", m youtube))
                        ]
