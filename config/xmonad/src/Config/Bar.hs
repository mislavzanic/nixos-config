{-# OPTIONS_GHC -Wno-missing-signatures #-}
{-# OPTIONS_GHC -Wno-orphans            #-}

{-# LANGUAGE ViewPatterns #-}

module Config.Bar where

import Theme.Xprop ( base08, base01 )

import XMonad ( ScreenId(..), X, Default(def) )

import XMonad.Hooks.DynamicLog (filterOutWsPP)
import XMonad.Hooks.StatusBar ( statusBarPropTo, StatusBarConfig )
import XMonad.Hooks.StatusBar.PP
    ( xmobarStrip,
      xmobarRaw,
      xmobarColor,
      wrap,
      shorten,
      PP(ppCurrent, ppTitleSanitize, ppSep, ppExtras, ppVisible,
         ppUrgent, ppHidden, ppOrder)
    )

import XMonad.Util.Loggers (logTitlesOnScreen)
import XMonad.Util.NamedScratchpad ( scratchpadWorkspaceTag )

baseXmobarConf :: ScreenId -> PP
baseXmobarConf sID = def
  { ppCurrent = xmobarColor base01 "" . wrap "[" "]"
  , ppVisible = xmobarColor base08 ""
  , ppTitleSanitize = xmobarStrip
  , ppSep = wrapInColor "#586E75" "  |  "
  , ppExtras = [ logTitlesOnScreen sID formatFocused formatUnfocused ]
  }
  where
    formatFocused, formatUnfocused :: String -> String
    formatFocused   = xmobarColor base01 "" . wrap "[" "]" . ppWindow
    formatUnfocused = xmobarColor base08 "" . wrap "[" "]" . ppWindow

    wrapInColor :: String -> String -> String
    wrapInColor color toWrap = "<fc=" ++ color ++ ">" ++ toWrap ++ "</fc>"

    ppWindow :: String -> String
    ppWindow = xmobarRaw
           . (\w -> if null w then "untitled" else w)
           . shorten 30
           . xmobarStrip

xmobarTop :: String -> StatusBarConfig
xmobarTop hostname = statusBarPropTo "_XMONAD_LOG_2" ( "bar -x 2 -h " <> hostname <> " -p top" ) xmobarTopPP

xmobarBottom :: ScreenId -> String -> StatusBarConfig
xmobarBottom sid@(S (show -> sn)) hostname = statusBarPropTo prop args (xmobarBottomPP sid)-- prop args
        where
          prop = "_XMONAD_LOG_" <> sn
          args = "bar -x "  <> sn <> " -h " <> hostname <> " -p bottom"

barSpawner :: String -> ScreenId -> StatusBarConfig
barSpawner hostname sid = case sid of
  0 -> xmobarTop hostname <> xmobarBottom 0 hostname
  1 -> xmobarBottom 1 hostname
  _ -> mempty

xmobarTopPP :: X PP
xmobarTopPP = pure $ filterOutWsPP [scratchpadWorkspaceTag] $ ( baseXmobarConf 0 )
  { ppHidden = xmobarColor base08 ""
  , ppVisible = xmobarColor base01 ""
  , ppUrgent = xmobarColor  base01 "" . wrap "!" "!"  -- Urgent workspace
  , ppOrder = \[ws, _, _, _] -> [ws]
  }

xmobarBottomPP :: ScreenId -> X PP
xmobarBottomPP sid = pure $ ( baseXmobarConf sid )
  { ppHidden = const ""
  , ppOrder = \[ws, l, _, wins] -> [ws, l, wins]
  }
