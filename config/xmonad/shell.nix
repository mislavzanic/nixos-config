{ pkgs ? import <nixpkgs> {} }:
with pkgs;
mkShell {
  buildInputs = [
    haskellPackages.cabal-install
    haskellPackages.haskell-language-server
    haskellPackages.hlint
    haskellPackages.ghcid
    haskellPackages.ormolu
    haskellPackages.implicit-hie
    haskellPackages.X11

    pkg-config

    xorg.libX11
    xorg.libX11.dev

    xorg.libXft
    xorg.libXext
    xorg.libXrandr
    xorg.libXrender
    xorg.libXinerama
    xorg.libXScrnSaver
  ];
  shellHook = ''
    export FLAKE="$(pwd)"
  '';
}
