{ inputs, config, lib, pkgs, ... }:

with lib;
with lib.my;
{

  imports =
    # I use home-manager to deploy files to $HOME; little else
    [ inputs.home-manager.nixosModules.home-manager ]
    # All my personal modules
    ++ (mapModulesRec' (toString ./modules) import);

  # Common config for all nixos machines; and to ensure the flake operates
  # soundly
  environment.variables.DOTFILES = config.dotfiles.dir;
  environment.variables.DOTFILES_BIN = config.dotfiles.binDir;
  environment.variables.NIXPKGS_ALLOW_UNFREE = "1";
  fileSystems."/".device = mkDefault "/dev/disk/by-label/nixos";

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      efi.canTouchEfiVariables = true;
      efi.efiSysMountPoint = "/boot/efi"; # ← use the same mount point here.
      systemd-boot.configurationLimit = 10;
      systemd-boot.enable = mkDefault true;
    };
    supportedFilesystems = [ "ntfs" ];
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  i18n.defaultLocale = "en_US.UTF-8";

  environment.systemPackages = with pkgs; [
    vim
    zsh
    git
    mpv
    wget
    feh
    brave
    firefox
    zathura
    python3
    cmake
    gnumake
    gcc

    texlive.combined.scheme-full
    lxappearance

    autorandr
    srandrd

    direnv
  ];

  services = {
    lorri.enable = true;
  };

  fonts.fonts = with pkgs; [
    nerdfonts
    cantarell-fonts
  ];

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  nix = {
    gc = {
      automatic = true;
      dates = "monthly";
      options = "--delete-older-than 30d";
    };

    settings = {
      auto-optimise-store = true;
    };

    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

}
