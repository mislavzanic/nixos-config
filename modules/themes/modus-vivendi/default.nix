{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.theme;
in {
  config = mkIf (cfg.active == "modus-vivendi") (mkMerge [
    {
      modules = {
        theme = {
          wallpaper = mkDefault ./config/fall.jpg;
          loginWallpaper = mkDefault ./config/fall.jpg;
          colors = {
            black         = "#000000";
            red           = "#ff8059"; # 1
            green         = "#44bc44"; # 2
            yellow        = "#d0bc00"; # 3
            blue          = "#2fafff"; # 4
            magenta       = "#feacd0"; # 5
            cyan          = "#00d3d0"; # 6
            silver        = "#bfbfbf"; # 7
            grey          = "#595959"; # 8
            brightred     = "#ef8b50"; # 9
            brightgreen   = "#70b900"; # 10
            brightyellow  = "#c0c530"; # 11
            brightblue    = "#79a8ff"; # 12
            brightmagenta = "#b6a0ff"; # 13
            brightcyan    = "#6ae4b9"; # 14
            white         = "#ffffff"; # 15


            types = {
              fg        = cfg.colors.white;
              bg        = cfg.colors.black;
              border    = "#1d2021";
              opacity   = "200";
            };
          };
        };
      };
    }

    (mkIf config.services.xserver.enable {
      fonts = {
        fonts = with pkgs; [
          fira-code
          fira-code-symbols
          open-sans
          jetbrains-mono
          siji
          font-awesome
          symbola
        ];
      };

      services.picom = {
        fade = false;
        shadow = false;
        settings = {
          blur-kern = "7x7box";
          blur-strength = 320;
        };
      };

      services.xserver.displayManager.lightdm.greeters.mini.extraConfig = ''
        text-color = "${cfg.colors.white}"
        password-background-color = "${cfg.colors.black}"
        window-color = "#303012"
        border-color = "${cfg.colors.types.border}"
        background-color = "#151230"
      '';

      home.configFile = with config.modules; mkMerge [
        (mkIf term.alacritty.enable {
          "alacritty/color.yml".text = with cfg.colors; ''
            colors:
              primary:
                background: '${types.bg}'
                foreground: '${types.fg}'

              normal:
                black:   '${black}'
                red:     '${red}'
                green:   '${green}'
                yellow:  '${yellow}'
                blue:    '${blue}'
                magenta: '${magenta}'
                cyan:    '${cyan}'
                white:   '${silver}'

              bright:
                black:   '${grey}'
                red:     '${brightred}'
                green:   '${brightgreen}'
                yellow:  '${brightyellow}'
                blue:    '${brightblue}'
                magenta: '${brightmagenta}'
                cyan:    '${brightcyan}'
                white:   '${white}'
          '';
        })
      ];
    })
  ]);
}
