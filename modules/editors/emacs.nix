{ config, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.editor.emacs;
  configDir = config.dotfiles.configDir;
in {

  options.modules.editor.emacs = {
    enable = mkBoolOpt false;
    doom = {
      enable = mkBoolOpt false;
      forgeUrl = mkOpt types.str "https://github.com";
      repoUrl = mkOpt types.str "${forgeUrl}/doomemacs/doomemacs";
      configUrl = mkOpt types.str "https://codeberg.org/mislavzanic/doom-config";
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [ inputs.emacs-overlay.overlay ];

    environment.systemPackages = with pkgs; [
      ((emacsPackagesFor emacs).emacsWithPackages (epkgs: [
        epkgs.vterm
      ]))

      binutils
      gnutls
      fd
      ripgrep
      jq
      imagemagick
      sqlite
      texlive.combined.scheme-medium
      xdotool
      xorg.xwininfo
    ];

    fonts.fonts = with pkgs; [
      emacs-all-the-icons-fonts
      jetbrains-mono
      nerdfonts
      cantarell-fonts
    ];

    system.userActivationScripts = mkIf cfg.doom.enable {
      installDoomEmacs = ''
        if [ ! -d "$XDG_CONFIG_HOME/emacs" ]; then
           git clone --depth=1 --single-branch "${cfg.doom.repoUrl}" "$XDG_CONFIG_HOME/emacs"
           git clone "${cfg.doom.configRepoUrl}" "$XDG_CONFIG_HOME/doom"
        fi
      '';
    };
  };
}
