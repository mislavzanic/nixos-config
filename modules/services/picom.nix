{ config, lib, pkgs, options, my, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.services.picom;
in {

  options.modules.services.picom = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.picom = {
      enable = true;
      activeOpacity = "0.90";

      settings = {
        blur = {
          method = "dual_kawase";
          size = 10;
        };

        opacityRule = [
          "100:class_g   *?= 'Chromium-browser'"
          "100:class_g   *?= 'Firefox'"
          "100:class_g   *?= 'emacs'"
          "100:class_g   *?= 'slack'"
        ];
      };

      experimentalBackends = true;
      vSync = true;
    };
  };
}
