{ options, config, pkgs, lib, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.desktop.xmonad;
  configDir = config.dotfiles.configDir;

in {

  options.modules.desktop.xmonad = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      xmonad-log
      haskellPackages.xmonad
      haskellPackages.mzanic-xmonad
      haskellPackages.mzanic-xmobar

      trayer
      networkmanagerapplet
      dmenu

      my.goaoc

      dunst
      libnotify

      maim
      pamixer
      pavucontrol
      pasystray
    ];

    services = {
      picom.enable = false;

      xserver = {
        enable = true;
        displayManager = {
          defaultSession = "none+myxmonad";
          sessionCommands = ''
            cat ~/.config/xtheme/* | '${pkgs.xorg.xrdb}/bin/xrdb' -load
          '';
          lightdm.enable = true;
          lightdm.greeters.mini = {
            enable = true;
            user = config.user.name;
          };
        };

        windowManager = {
          session = [{
            name = "myxmonad";
            start = ''
              /usr/bin/env mzanic-xmonad &
              waitPID=$!
            '';
          }];
        };
      };
    };

    home.configFile = {
      "xmonad" = {
        source    = "${configDir}/xmonad";
        recursive = true;
      };
    };
  };

}
