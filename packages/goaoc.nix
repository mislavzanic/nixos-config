{ lib, stdenv, buildGoModule, ... }:

buildGoModule rec {
  name = "goaoc";
  src = builtins.fetchGit {
    url = "https://codeberg.org/mislavzanic/goaoc";
    ref = "HEAD";
  };

  vendorSha256 = "sha256-ADi5Dj2Ll7ECnOnev5EHd1Lrp51Mtt/kfhOwj8VCXUU=";
}
