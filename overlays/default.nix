_: pkgs: rec {
  haskellPackages = pkgs.haskellPackages.override (old: {
    overrides = pkgs.lib.composeExtensions (old.overrides or (_: _: { }))
      (self: super: rec {
        mzanic-xmonad = self.callCabal2nix "mzanic-xmonad" ../config/xmonad { };
        mzanic-xmobar = self.callCabal2nix "mzanic-xmobar" ../config/xmobar { };
      });
  });
}
