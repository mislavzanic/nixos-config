#+TITLE: My NixOS dotfiles

This is my current NixOS configuration.

* Inspirations (or shameless copies :smile:)
- [[https://github.com/hlissner/dotfiles][Hlissner's dotfiles]]
- [[https://github.com/alternateved/nixos-config][alternatived's dotfiles]]
- [[https://codeberg.org/exorcist/dotfulls][exorcist/dotfulls]]
- [[https://gitlab.com/slotThe/dotfiles][slotThe's dotfiles]]
- [[https://github.com/liskin/dotfiles][liskin/dotfiles]]

* XMonad
#+NAME: fig:XMonad config
[[./config/screenshot.png]]
